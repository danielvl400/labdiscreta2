#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdbool.h>

#define MAX_CARACTER 81

/* -----------------  ESTRUCTURA VERTICE  ------------------------- */

struct _vertice
{
    unsigned int nombre_vertice;
    unsigned int color;
    unsigned int cantidad_vecinos;
    unsigned int *vecinos;
    unsigned int sig;
}verticeT;

// E AQUI NUESTRO PROBLEMA CREO
typedef struct _vertice *Tabla;

typedef unsigned int U32;

/* -----------------  ESTRUCTURA VERTICE  ------------------------- */


int compare(const void *a, const void *b)
{
    const unsigned int *x = a, *y = b;
    if(*x > *y)
        return 1;
    else
        return (*x < *y) ? -1 : 0;
}

int busqueda_binaria(unsigned int **arreglo, unsigned int t, unsigned int x){
    int izq = 0;
    int der = t - 1;
    int medio = -1;
    //printf(" valor que busco  %u\n", x);
    while(izq <= der){
        medio = (izq + der) / 2;
        if (arreglo[0][medio] == x){
            //printf("%u es igual \n", arreglo[0][medio]);
            return medio;
        } 
        else if (arreglo[0][medio] > x){
            der = medio-1;
            //printf("%u es mas chico \n", arreglo[0][medio]);
        } 
        else {
            izq = medio+1;
            //printf("%u es mas grande \n", arreglo[0][medio]);
        }
    }
    return -1;
}

int  main()
{
    unsigned int vertice, lado;
    unsigned int cvertices, clados;
    unsigned int * arreglo_total;
    unsigned int **arreglo_vertices;
    char line[MAX_CARACTER];
    bool is_p = false;
    unsigned int i, j = 0;
    unsigned int cantidad_v = 0;
    unsigned int pos_ver;
    unsigned int pos_lado;

    Tabla *array;

    /* read at least 80 characters or unitl newline charater is encountered with */
    /*    fgets(line, sizeof line, stdin) */
    /* if the first character is a newline, then it's an empty line of input */
    printf("%s\n","recorro el archivo y obtengo cantidad de vertices y aristas");
    while ((line[0] != '\n' && !is_p) && (fgets(line, sizeof line, stdin) != NULL))
    {
        /* parse the read line with sscanf */
        if (line[0] != 'c' && line[0] == 'p')
        {
            if (sscanf(line, "p edge %u %u", &cvertices, &clados) == 2)
            {
                is_p = true;
                fflush(stdout);
            }
        }

    }
    printf("%s\n","cargo todos los vertices" );
    /*   --------------------- creo el arreglo que necesito --------------*/
    arreglo_total = (unsigned int*)malloc ((clados*2)*sizeof(unsigned int) );
    i = 0;
    while(i < (clados))
    {
        if ((fgets(line, sizeof line, stdin) != NULL) && (line[0] != '\n'))
        {
            if (line[0] == 'e' && sscanf(line, "e %u %u", &vertice, &lado) == 2) {
                arreglo_total[j] = (U32)vertice;
                arreglo_total[j+1] = (U32)lado;
                j = j + 2;
                fflush(stdout);
                i++;
            }
        }
    }

    /*---------------------------------------------------------------------------*/


    /* ----------- TENGO QUE ORDENAR EL ARREGLO CON TODOS LOS VECINOS ---------- */
    printf("%s\n","ordeno todo los vertices" );
    qsort(arreglo_total, (clados*2), sizeof(unsigned int), &compare);


    for (unsigned int i = 0; i < (clados*2); i++)
    {
        //printf("%u\n", arreglo_total[i]);
        j = arreglo_total[i];
    }
    //printf("%u\n", arreglo_total[i]); 


    /* ------------------------------------------------------------------------- */


    /*---- CREO LA MATRIZ PARA GUARDAR EL VERTICE Y LA CANTIDAD DE VESINOS ------*/
    printf("%s\n","creo una matris para los vertices y sus repeticiones mas un dato adicional");
    arreglo_vertices = calloc(3, sizeof(unsigned int *));
 
    for (unsigned int f = 0; f < 3; f++) {
        arreglo_vertices[f] = calloc(cvertices+1, sizeof(unsigned int));
    }
    /* -------------------------------------------------------------------------- */

    for (unsigned int i = 0; i < (clados * 2)-1; i++)
    {
        //printf("%u\n", arreglo_total[i]);
        if (arreglo_total[i] > arreglo_total[i+1])
        {
            printf("%s %u - %u \\ %u - %u\n", "error",i,arreglo_total[i],(i+1), arreglo_total[i+1]);
            return 0;
        }

           /* if (arreglo_total[i] == 2649511045)
            {
                printf("%u\n", i);
                return 0;
            } */

    }
    //printf("%u\n", (clados * 2));


//    printf("%s\n","***********************************************");

    printf("%s\n", "cargo dicho arreglo");
    j = 0;
    cantidad_v = 1;
    unsigned int cantidad = (clados*2);

    printf("cantidad %u \n",cantidad);
    for (i = 0; i < cantidad; i++) {

        if (i != cantidad-1){
            //printf("comparo [%u] -> %u con [%u]-> %u \n",i,arreglo_total[i],i+1,arreglo_total[i+1] );
            if(arreglo_total[i]==arreglo_total[i+1]){
                cantidad_v++;
            } else {
                arreglo_vertices[0][j] = arreglo_total[i];
                arreglo_vertices[1][j] = cantidad_v;
                j++;
                cantidad_v = 1;
            }

        } else {
            printf("#### %u <- i %u <- j ####\n",i,j);
            printf("comparo [%u] -> %u con [%u]-> %u \n",i,arreglo_total[i],i+1,arreglo_total[i+1] );

            if(arreglo_total[i] == arreglo_total[i+1]){
                arreglo_vertices[0][j+1] = arreglo_total[i];
                arreglo_vertices[1][j+1] = cantidad_v;
            } else {
                arreglo_vertices[0][j] = arreglo_total[i];
                arreglo_vertices[1][j] = cantidad_v;
                arreglo_vertices[0][j+1] = arreglo_total[i+1];
                arreglo_vertices[1][j+1] = 1;
            }
        }



    }




    //for (i = 0; i < cvertices; ++i)
   // {
       // printf("%u | %u  %u  %u\n",i,arreglo_vertices[0][i], arreglo_vertices[1][i], arreglo_vertices[2][i]);
   // }


/*
    int e;
    for ( i = 0; i <= cvertices; ++i)
    {   
        printf("%s %u\n","buscando",arreglo_vertices[0][i]);
        e = busqueda_binaria(arreglo_vertices,cvertices,arreglo_vertices[0][i]);
        printf("%d indice donde esta el %u\n",e,arreglo_vertices[0][i]);
        if (e == -1)
        {
            printf("%s\n","ocurrio un error");
            return 0;
        }
    }
*/

    printf("%s\n", "regreso al principio del la entrada standar");
    (void) fseek(stdin, 0L, SEEK_SET);


    /* ------------------------------------------------ */
    // con esto creo el arreglo de vertices
    array = malloc((cvertices) * sizeof(Tabla));
    for (unsigned int i = 0; i < cvertices; ++i)
    {
        array[i] = NULL;
        array[i] = malloc(sizeof(verticeT));
    }
    /*  ----------------------------------------------  */

    printf("%s\n","EN ESTE MOMENTO ESTA CREANDO LA ESTRUCTURA" );
    while ((fgets(line, sizeof line, stdin) != NULL))

    {
        if (line[0] == 'e')
        {
            if (sscanf(line, "e %u %u", &vertice, &lado) == 2)
            {
                vertice=vertice;
                lado=lado;
                //printf("[%u]  [%u] los vertices son\n",vertice,lado);
                pos_ver = busqueda_binaria(arreglo_vertices,cvertices,vertice);
                pos_lado = busqueda_binaria(arreglo_vertices,cvertices,lado);

                // si el vertice no existe lo agrego a la estructura
                // en la pocision que le corresponde dentro del arreglo de vertices
                //printf("%d   %d los indices de los vertices \n",pos_ver,pos_lado);


                //printf("el primer vertice esta cargado =? %u \n",arreglo_verices[2][pos_ver]);
                unsigned int e1 = arreglo_vertices[1][pos_ver];
                //printf("%u\n",arreglo_vertices[1][pos_lado]);
                unsigned int e2 = arreglo_vertices[1][pos_lado];


                if (arreglo_vertices[2][pos_ver] == 0)
                {
                    // nombre de vertice
                    //printf("%s   -> %u\n", "la concha de tu madre cuanto vale el pos_ver",pos_ver);
                    array[pos_ver]->nombre_vertice = vertice;
                    //printf("carga el nombre?\n");
                    // color del mismo
                    // por ahora no tiene color a si que le ponemos 0
                    array[pos_ver]->color = 0;
                    //printf("carga el color?\n");
                    // esto lo obtengo de la otra matriz
                    array[pos_ver]->cantidad_vecinos = arreglo_vertices[1][pos_ver];
                    //printf("carga la cantidad de vecinos?\n");
                    // creamos el arreglo justo para sus vecinos
                    array[pos_ver]->vecinos = malloc(e1 * sizeof(unsigned int));
                    //printf("carga el arreglo para los vecinos?\n");
                    // y por ultimo inicializo una variable que me va a ayudar
                    // a poner al vecino donde corresponda
                    array[pos_ver]->sig = 0;
                    //printf("carga el valor de sig?\n");
                    // por ultimo lo marcamos como cargado en la matriz de vertices "arreglo_vertices"
                    arreglo_vertices[2][pos_ver] = 1;
                    //printf("modifica el valor de la matriz?\n");
                }

                /*---- esto se puede mejorar un monton pero me vale quiero ser analista no licenciado :D */
                // en este caso pasa lo mismo con el otro vertice asi que me fijo si esta creado
                // y si no lo esta pos lo creo y ya

                //printf("el segundo vertice esta pre cargado =? %u \n",arreglo_verices[2][pos_lado]);

                if (arreglo_vertices[2][pos_lado] == 0)
                {
                    // nombre de otro vertice
                    array[pos_lado]->nombre_vertice = lado;
                    // color del mismo
                    // por ahora no tiene color a si que le ponemos 0
                    array[pos_lado]->color = 0;
                    // esto lo obtengo de la otra matriz
                    array[pos_lado]->cantidad_vecinos = arreglo_vertices[1][pos_lado];
                    // creamos el arreglo justo para sus vecinos
                    array[pos_lado]->vecinos = malloc(e2 * sizeof(unsigned int));
                    // y por ultimo inicializo una variable que me va a ayudar
                    // a poner al vecino donde corresponda
                    array[pos_lado]->sig = 0;
                    // por ultimo lo marcamos como cargado en la matriz de vertices "arreglo_vertices"
                    arreglo_vertices[2][pos_lado] = 1;
                }

                /*
                    una vez creado los dos o alguno de los dos o ninguno de los dos vertices 
                    lo unico que tengo que hacer es colocar dentro del arreglo de vecinos de cada 
                    uno de los vertices el id del otro y aumentar el contador de "sig" en uno
                */

                /* ------------------------------------------------------------------------------------ */
                
                array[pos_ver]->vecinos[array[pos_ver]->sig] = pos_lado;

                //si ya se que se puede usar el variable++ pero no me voy a arriesgar 
                array[pos_ver]->sig = array[pos_ver]->sig + 1;

                array[pos_lado]->vecinos[array[pos_lado]->sig] = pos_ver;

                // lo mismo en este caso
                array[pos_lado]->sig = array[pos_lado]->sig + 1;


                //printf("%u\n", tabla[pos_lado].VERTICE->nombre_vertice);

                /* ------------------------------------------------------------------------------------ */


                //printf("%u   %u \n",pos_ver,pos_lado);
                fflush(stdout);
            }
        }

    }


    printf("%s\n","fin");
/*
    for (unsigned int i = 0; i < cvertices; ++i)
    {

        printf("[vr = %u, c= %u, cv = %u] -> [",array[i]->nombre_vertice, array[i]->color, array[i]->cantidad_vecinos);
        for (unsigned int j = 0; j < array[i]->cantidad_vecinos; ++j)
        {
            printf("%u ", array[i]->vecinos[j]);
        }
        printf("]\n");
    }
*/


    printf("%u %u\n", cvertices, clados);
    return 0;
}
