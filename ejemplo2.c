#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdbool.h>

/* -----------------  ESTRUCTURA VERTICE  ------------------------- */
typedef struct _vertice
{
    unsigned int nombre_vertice;
    unsigned int color;
    unsigned int cantidad_vecinos;
    unsigned int *vecinos;
    unsigned int sig;
}verticeT;

typedef struct _vertice *Tabla;
/* -----------------  ESTRUCTURA VERTICE  ------------------------- */


int main() {

    Tabla *array; 
    array = malloc(10 * sizeof(Tabla));

    for (unsigned int i = 0; i < 10; ++i)
    {
        array[i] = NULL;
        array[i] = malloc(sizeof(verticeT));
        array[i]->nombre_vertice = i;
        array[i]->color = 2*i;
        array[i]->cantidad_vecinos = 3*i;
        array[i]->sig = i+1;
        array[i]->vecinos = malloc((i+1) * sizeof(unsigned int));

        for (unsigned int x = 0; x <= i; ++x)
        {
            array[i]->vecinos[x] = i; 
        }

    }


    for (unsigned int i = 0; i < 10; ++i)
    {
        printf("%u  <-- nombre vertice\n", array[i]->nombre_vertice);
        printf("%u  <-- color \n", array[i]->color);
        printf("%u  <-- cantidad_vecinos\n", array[i]->cantidad_vecinos);
        printf("%u  <-- sig\n", array[i]->sig);
        for (unsigned int j = 0; j <= i; ++j)
        {
            printf("%u  << ", array[i]->vecinos[j]);
        }
        printf("\n");
    }


    return 0;
}
